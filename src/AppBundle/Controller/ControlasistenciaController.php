<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\Controlasistencia;
use AppBundle\Form\ControlasistenciaType;

/**
 * Controlasistencia controller.
 *
 * @Route("/controlasistencia")
 */
class ControlasistenciaController extends Controller
{

    /**
     * Lists all Controlasistencia entities.
     *
     * @Route("/", name="controlasistencia")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:Controlasistencia')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Controlasistencia entity.
     *
     * @Route("/", name="controlasistencia_create")
     * @Method("POST")
     * @Template("AppBundle:Controlasistencia:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Controlasistencia();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('controlasistencia_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Controlasistencia entity.
     *
     * @param Controlasistencia $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Controlasistencia $entity)
    {
        $form = $this->createForm(new ControlasistenciaType(), $entity, array(
            'action' => $this->generateUrl('controlasistencia_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Controlasistencia entity.
     *
     * @Route("/new", name="controlasistencia_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Controlasistencia();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Controlasistencia entity.
     *
     * @Route("/{id}", name="controlasistencia_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Controlasistencia')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Controlasistencia entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Controlasistencia entity.
     *
     * @Route("/{id}/edit", name="controlasistencia_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Controlasistencia')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Controlasistencia entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Controlasistencia entity.
    *
    * @param Controlasistencia $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Controlasistencia $entity)
    {
        $form = $this->createForm(new ControlasistenciaType(), $entity, array(
            'action' => $this->generateUrl('controlasistencia_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Controlasistencia entity.
     *
     * @Route("/{id}", name="controlasistencia_update")
     * @Method("PUT")
     * @Template("AppBundle:Controlasistencia:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Controlasistencia')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Controlasistencia entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('controlasistencia_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Controlasistencia entity.
     *
     * @Route("/{id}", name="controlasistencia_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Controlasistencia')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Controlasistencia entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('controlasistencia'));
    }

    /**
     * Creates a form to delete a Controlasistencia entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('controlasistencia_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
