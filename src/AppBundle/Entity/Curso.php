<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Curso
 *
 * @ORM\Table(name="Curso", indexes={@ORM\Index(name="fkCursoProyecto1idx", columns={"ProyectoidProyecto"})})
 * @ORM\Entity
 */
class Curso
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idCurso", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idcurso;

    /**
     * @var string
     *
     * @ORM\Column(name="cursNombre", type="string", length=45, nullable=false)
     */
    private $cursnombre;

    /**
     * @var \Proyecto
     *
     * @ORM\ManyToOne(targetEntity="Proyecto")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ProyectoidProyecto", referencedColumnName="idProyecto")
     * })
     */
    private $proyectoidproyecto;



    /**
     * Get idcurso
     *
     * @return integer
     */
    public function getIdcurso()
    {
        return $this->idcurso;
    }

    /**
     * Set cursnombre
     *
     * @param string $cursnombre
     *
     * @return Curso
     */
    public function setCursnombre($cursnombre)
    {
        $this->cursnombre = $cursnombre;

        return $this;
    }

    /**
     * Get cursnombre
     *
     * @return string
     */
    public function getCursnombre()
    {
        return $this->cursnombre;
    }

    /**
     * Set proyectoidproyecto
     *
     * @param \AppBundle\Entity\Proyecto $proyectoidproyecto
     *
     * @return Curso
     */
    public function setProyectoidproyecto(\AppBundle\Entity\Proyecto $proyectoidproyecto = null)
    {
        $this->proyectoidproyecto = $proyectoidproyecto;

        return $this;
    }

    /**
     * Get proyectoidproyecto
     *
     * @return \AppBundle\Entity\Proyecto
     */
    public function getProyectoidproyecto()
    {
        return $this->proyectoidproyecto;
    }
}
