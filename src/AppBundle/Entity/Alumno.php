<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Alumno
 *
 * @ORM\Table(name="Alumno", indexes={@ORM\Index(name="fkAlumnoCursoidx", columns={"CursoidCurso"})})
 * @ORM\Entity
 */
class Alumno
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idAlumno", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idalumno;

    /**
     * @var string
     *
     * @ORM\Column(name="alumNombres", type="string", length=45, nullable=false)
     */
    private $alumnombres;

    /**
     * @var string
     *
     * @ORM\Column(name="alumApellidos", type="string", length=45, nullable=false)
     */
    private $alumapellidos;

    /**
     * @var \Curso
     *
     * @ORM\ManyToOne(targetEntity="Curso")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="CursoidCurso", referencedColumnName="idCurso")
     * })
     */
    private $cursoidcurso;


}

