<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Grupoalimenticio
 *
 * @ORM\Table(name="GrupoAlimenticio")
 * @ORM\Entity
 */
class Grupoalimenticio
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idGrupoAlimenticio", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idgrupoalimenticio;

    /**
     * @var string
     *
     * @ORM\Column(name="grupNombre", type="string", length=45, nullable=false)
     */
    private $grupnombre;

    /**
     * @var string
     *
     * @ORM\Column(name="grupDescripcion", type="string", length=45, nullable=false)
     */
    private $grupdescripcion;


}

