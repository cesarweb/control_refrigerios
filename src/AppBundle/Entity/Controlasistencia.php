<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Controlasistencia
 *
 * @ORM\Table(name="ControlAsistencia", indexes={@ORM\Index(name="fkControlAsistenciaInventarioBodega1idx", columns={"InventarioBodegaidInventarioB"}), @ORM\Index(name="fkControlAsistenciaCurso1idx", columns={"CursoidCurso"})})
 * @ORM\Entity
 */
class Controlasistencia
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idControlAsistencia", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idcontrolasistencia;

    /**
     * @var integer
     *
     * @ORM\Column(name="NumeroAsistentes", type="integer", nullable=false)
     */
    private $numeroasistentes;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="FechaAsistencia", type="datetime", nullable=false)
     */
    private $fechaasistencia;

    /**
     * @var \Inventariobodega
     *
     * @ORM\ManyToOne(targetEntity="Inventariobodega")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="InventarioBodegaidInventarioB", referencedColumnName="idInventarioBodega")
     * })
     */
    private $inventariobodegaidinventariob;

    /**
     * @var \Curso
     *
     * @ORM\ManyToOne(targetEntity="Curso")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="CursoidCurso", referencedColumnName="idCurso")
     * })
     */
    private $cursoidcurso;



    /**
     * Get idcontrolasistencia
     *
     * @return integer
     */
    public function getIdcontrolasistencia()
    {
        return $this->idcontrolasistencia;
    }

    /**
     * Set numeroasistentes
     *
     * @param integer $numeroasistentes
     *
     * @return Controlasistencia
     */
    public function setNumeroasistentes($numeroasistentes)
    {
        $this->numeroasistentes = $numeroasistentes;

        return $this;
    }

    /**
     * Get numeroasistentes
     *
     * @return integer
     */
    public function getNumeroasistentes()
    {
        return $this->numeroasistentes;
    }

    /**
     * Set fechaasistencia
     *
     * @param \DateTime $fechaasistencia
     *
     * @return Controlasistencia
     */
    public function setFechaasistencia($fechaasistencia)
    {
        $this->fechaasistencia = $fechaasistencia;

        return $this;
    }

    /**
     * Get fechaasistencia
     *
     * @return \DateTime
     */
    public function getFechaasistencia()
    {
        return $this->fechaasistencia;
    }

    /**
     * Set inventariobodegaidinventariob
     *
     * @param \AppBundle\Entity\Inventariobodega $inventariobodegaidinventariob
     *
     * @return Controlasistencia
     */
    public function setInventariobodegaidinventariob(\AppBundle\Entity\Inventariobodega $inventariobodegaidinventariob = null)
    {
        $this->inventariobodegaidinventariob = $inventariobodegaidinventariob;

        return $this;
    }

    /**
     * Get inventariobodegaidinventariob
     *
     * @return \AppBundle\Entity\Inventariobodega
     */
    public function getInventariobodegaidinventariob()
    {
        return $this->inventariobodegaidinventariob;
    }

    /**
     * Set cursoidcurso
     *
     * @param \AppBundle\Entity\Curso $cursoidcurso
     *
     * @return Controlasistencia
     */
    public function setCursoidcurso(\AppBundle\Entity\Curso $cursoidcurso = null)
    {
        $this->cursoidcurso = $cursoidcurso;

        return $this;
    }

    /**
     * Get cursoidcurso
     *
     * @return \AppBundle\Entity\Curso
     */
    public function getCursoidcurso()
    {
        return $this->cursoidcurso;
    }
}
