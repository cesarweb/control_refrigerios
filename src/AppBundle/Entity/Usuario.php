<?php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * Usuario
 *
 * @ORM\Table(name="Usuario", indexes={@ORM\Index(name="fkUsuarioPerfil1idx", columns={"PerfilidPerfil"})})
 * @ORM\Entity
 */
class Usuario extends BaseUser
{

    public function __construct()
    {
        parent::__construct();
        // your own logic
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var \Perfil
     *
     * @ORM\ManyToOne(targetEntity="Perfil")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="PerfilidPerfil", referencedColumnName="idPerfil")
     * })
     */
    private $perfilperfil;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set perfilperfil
     *
     * @param \ControlRefrigeriosBundle\Entity\Perfil $perfilperfil
     *
     * @return Usuario
     */
    public function setPerfilperfil(\ControlRefrigeriosBundle\Entity\Perfil $perfilperfil = null)
    {
        $this->perfilperfil = $perfilperfil;

        return $this;
    }

    /**
     * Get perfilperfil
     *
     * @return \ControlRefrigeriosBundle\Entity\Perfil
     */
    public function getPerfilperfil()
    {
        return $this->perfilperfil;
    }
}