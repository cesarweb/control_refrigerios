<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Usuariocurso
 *
 * @ORM\Table(name="UsuarioCurso", indexes={@ORM\Index(name="fkUsuariohasCursoCurso1idx", columns={"CursoidCurso"}), @ORM\Index(name="fkUsuarioCursoUsuario1idx", columns={"UsuarioidUsuario"})})
 * @ORM\Entity
 */
class Usuariocurso
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idUsuarioCurso", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idusuariocurso;

    /**
     * @var \Curso
     *
     * @ORM\ManyToOne(targetEntity="Curso")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="CursoidCurso", referencedColumnName="idCurso")
     * })
     */
    private $cursoidcurso;

    /**
     * @var \Usuario
     *
     * @ORM\ManyToOne(targetEntity="Usuario")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="UsuarioidUsuario", referencedColumnName="id")
     * })
     */
    private $usuarioidusuario;


}

