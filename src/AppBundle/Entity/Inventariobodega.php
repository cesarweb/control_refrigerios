<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Inventariobodega
 *
 * @ORM\Table(name="InventarioBodega", indexes={@ORM\Index(name="fkInventarioBodegaProyecto1idx", columns={"ProyectoidProyecto"})})
 * @ORM\Entity
 */
class Inventariobodega
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idInventarioBodega", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idinventariobodega;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Fechallegada", type="datetime", nullable=false)
     */
    private $fechallegada;

    /**
     * @var integer
     *
     * @ORM\Column(name="NumeroRefrigerios", type="integer", nullable=false)
     */
    private $numerorefrigerios;

    /**
     * @var \Proyecto
     *
     * @ORM\ManyToOne(targetEntity="Proyecto")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ProyectoidProyecto", referencedColumnName="idProyecto")
     * })
     */
    private $proyectoidproyecto;



    /**
     * Get idinventariobodega
     *
     * @return integer
     */
    public function getIdinventariobodega()
    {
        return $this->idinventariobodega;
    }

    /**
     * Set fechallegada
     *
     * @param \DateTime $fechallegada
     *
     * @return Inventariobodega
     */
    public function setFechallegada($fechallegada)
    {
        $this->fechallegada = $fechallegada;

        return $this;
    }

    /**
     * Get fechallegada
     *
     * @return \DateTime
     */
    public function getFechallegada()
    {
        return $this->fechallegada;
    }

    /**
     * Set numerorefrigerios
     *
     * @param integer $numerorefrigerios
     *
     * @return Inventariobodega
     */
    public function setNumerorefrigerios($numerorefrigerios)
    {
        $this->numerorefrigerios = $numerorefrigerios;

        return $this;
    }

    /**
     * Get numerorefrigerios
     *
     * @return integer
     */
    public function getNumerorefrigerios()
    {
        return $this->numerorefrigerios;
    }

    /**
     * Set proyectoidproyecto
     *
     * @param \AppBundle\Entity\Proyecto $proyectoidproyecto
     *
     * @return Inventariobodega
     */
    public function setProyectoidproyecto(\AppBundle\Entity\Proyecto $proyectoidproyecto = null)
    {
        $this->proyectoidproyecto = $proyectoidproyecto;

        return $this;
    }

    /**
     * Get proyectoidproyecto
     *
     * @return \AppBundle\Entity\Proyecto
     */
    public function getProyectoidproyecto()
    {
        return $this->proyectoidproyecto;
    }
}
