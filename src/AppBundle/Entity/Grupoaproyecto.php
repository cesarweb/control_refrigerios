<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Grupoaproyecto
 *
 * @ORM\Table(name="GrupoAProyecto", indexes={@ORM\Index(name="fkGrupoAProyectoGrupoAlimenticio1idx", columns={"GrupoAlimenticioidGrupoA"}), @ORM\Index(name="fkGrupoAProyectoProyecto1idx", columns={"ProyectoidProyecto"})})
 * @ORM\Entity
 */
class Grupoaproyecto
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idGrupoAProyecto", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idgrupoaproyecto;

    /**
     * @var \Proyecto
     *
     * @ORM\ManyToOne(targetEntity="Proyecto")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ProyectoidProyecto", referencedColumnName="idProyecto")
     * })
     */
    private $proyectoidproyecto;

    /**
     * @var \Grupoalimenticio
     *
     * @ORM\ManyToOne(targetEntity="Grupoalimenticio")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="GrupoAlimenticioidGrupoA", referencedColumnName="idGrupoAlimenticio")
     * })
     */
    private $grupoalimenticioidgrupoa;


}

