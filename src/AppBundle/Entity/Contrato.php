<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Contrato
 *
 * @ORM\Table(name="Contrato")
 * @ORM\Entity
 */
class Contrato
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idContrato", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idcontrato;

    /**
     * @var integer
     *
     * @ORM\Column(name="TotalRefrigerios", type="integer", nullable=false)
     */
    private $totalrefrigerios;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Anio", type="date", nullable=false)
     */
    private $anio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="FechaVencimiento", type="date", nullable=false)
     */
    private $fechavencimiento;



    /**
     * Get idcontrato
     *
     * @return integer
     */
    public function getIdcontrato()
    {
        return $this->idcontrato;
    }

    /**
     * Set totalrefrigerios
     *
     * @param integer $totalrefrigerios
     *
     * @return Contrato
     */
    public function setTotalrefrigerios($totalrefrigerios)
    {
        $this->totalrefrigerios = $totalrefrigerios;

        return $this;
    }

    /**
     * Get totalrefrigerios
     *
     * @return integer
     */
    public function getTotalrefrigerios()
    {
        return $this->totalrefrigerios;
    }

    /**
     * Set anio
     *
     * @param \DateTime $anio
     *
     * @return Contrato
     */
    public function setAnio($anio)
    {
        $this->anio = $anio;

        return $this;
    }

    /**
     * Get anio
     *
     * @return \DateTime
     */
    public function getAnio()
    {
        return $this->anio;
    }

    /**
     * Set fechavencimiento
     *
     * @param \DateTime $fechavencimiento
     *
     * @return Contrato
     */
    public function setFechavencimiento($fechavencimiento)
    {
        $this->fechavencimiento = $fechavencimiento;

        return $this;
    }

    /**
     * Get fechavencimiento
     *
     * @return \DateTime
     */
    public function getFechavencimiento()
    {
        return $this->fechavencimiento;
    }
}
