-- phpMyAdmin SQL Dump
-- version 4.5.2deb2.trusty~ppa.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 22-12-2015 a las 10:28:31
-- Versión del servidor: 5.6.27-0ubuntu0.14.04.1
-- Versión de PHP: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `control_refrigerios`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Alumno`
--

CREATE TABLE `Alumno` (
  `idAlumno` int(11) NOT NULL,
  `alumNombres` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `alumApellidos` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `CursoidCurso` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Contrato`
--

CREATE TABLE `Contrato` (
  `idContrato` int(11) NOT NULL,
  `TotalRefrigerios` int(11) NOT NULL,
  `Anio` date NOT NULL,
  `FechaVencimiento` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `Contrato`
--

INSERT INTO `Contrato` (`idContrato`, `TotalRefrigerios`, `Anio`, `FechaVencimiento`) VALUES
(1, 1486, '2014-07-01', '2015-12-01');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ControlAsistencia`
--

CREATE TABLE `ControlAsistencia` (
  `idControlAsistencia` int(11) NOT NULL,
  `NumeroAsistentes` int(11) NOT NULL,
  `FechaAsistencia` datetime NOT NULL,
  `CursoidCurso` int(11) DEFAULT NULL,
  `InventarioBodegaidInventarioB` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Curso`
--

CREATE TABLE `Curso` (
  `idCurso` int(11) NOT NULL,
  `cursNombre` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `ProyectoidProyecto` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `GrupoAlimenticio`
--

CREATE TABLE `GrupoAlimenticio` (
  `idGrupoAlimenticio` int(11) NOT NULL,
  `grupNombre` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `grupDescripcion` varchar(45) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `GrupoAProyecto`
--

CREATE TABLE `GrupoAProyecto` (
  `idGrupoAProyecto` int(11) NOT NULL,
  `GrupoAlimenticioidGrupoA` int(11) DEFAULT NULL,
  `ProyectoidProyecto` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `InventarioBodega`
--

CREATE TABLE `InventarioBodega` (
  `idInventarioBodega` int(11) NOT NULL,
  `Fechallegada` datetime NOT NULL,
  `NumeroRefrigerios` int(11) NOT NULL,
  `ProyectoidProyecto` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Perfil`
--

CREATE TABLE `Perfil` (
  `idPerfil` int(11) NOT NULL,
  `perfRol` varchar(45) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Proyecto`
--

CREATE TABLE `Proyecto` (
  `idProyecto` int(11) NOT NULL,
  `TipoProyecto` varchar(45) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Usuario`
--

CREATE TABLE `Usuario` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  `PerfilidPerfil` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `Usuario`
--

INSERT INTO `Usuario` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `locked`, `expired`, `expires_at`, `confirmation_token`, `password_requested_at`, `roles`, `credentials_expired`, `credentials_expire_at`, `PerfilidPerfil`) VALUES
(1, 'root', 'root', 'cesitar@soychevere.oh', 'cesitar@soychevere.oh', 1, '23jl5ngaq3vo00cwkkogcsscwgsscwg', '$2y$13$SFSGTpCw3zYuFXXNZz4sd.aW6DSmGiR.BIu4XnhZDnCbuQUDKHoo.', '2015-12-19 12:15:44', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, NULL),
(2, 'cesar', 'cesar', 'cesar@timemanagerweb.com', 'cesar@timemanagerweb.com', 1, '6kauy3yzcww0sk8kwwks8kswck0gw0w', '$2y$13$6kauy3yzcww0sk8kwwks8etRfO3ICT95QhC6Yx2jnxO5xRJsPJcNi', '2015-12-19 12:19:19', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `UsuarioCurso`
--

CREATE TABLE `UsuarioCurso` (
  `idUsuarioCurso` int(11) NOT NULL,
  `UsuarioidUsuario` int(11) DEFAULT NULL,
  `CursoidCurso` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `Alumno`
--
ALTER TABLE `Alumno`
  ADD PRIMARY KEY (`idAlumno`),
  ADD KEY `fkAlumnoCursoidx` (`CursoidCurso`);

--
-- Indices de la tabla `Contrato`
--
ALTER TABLE `Contrato`
  ADD PRIMARY KEY (`idContrato`);

--
-- Indices de la tabla `ControlAsistencia`
--
ALTER TABLE `ControlAsistencia`
  ADD PRIMARY KEY (`idControlAsistencia`),
  ADD KEY `fkControlAsistenciaInventarioBodega1idx` (`InventarioBodegaidInventarioB`),
  ADD KEY `fkControlAsistenciaCurso1idx` (`CursoidCurso`);

--
-- Indices de la tabla `Curso`
--
ALTER TABLE `Curso`
  ADD PRIMARY KEY (`idCurso`),
  ADD KEY `fkCursoProyecto1idx` (`ProyectoidProyecto`);

--
-- Indices de la tabla `GrupoAlimenticio`
--
ALTER TABLE `GrupoAlimenticio`
  ADD PRIMARY KEY (`idGrupoAlimenticio`);

--
-- Indices de la tabla `GrupoAProyecto`
--
ALTER TABLE `GrupoAProyecto`
  ADD PRIMARY KEY (`idGrupoAProyecto`),
  ADD KEY `fkGrupoAProyectoGrupoAlimenticio1idx` (`GrupoAlimenticioidGrupoA`),
  ADD KEY `fkGrupoAProyectoProyecto1idx` (`ProyectoidProyecto`);

--
-- Indices de la tabla `InventarioBodega`
--
ALTER TABLE `InventarioBodega`
  ADD PRIMARY KEY (`idInventarioBodega`),
  ADD KEY `fkInventarioBodegaProyecto1idx` (`ProyectoidProyecto`);

--
-- Indices de la tabla `Perfil`
--
ALTER TABLE `Perfil`
  ADD PRIMARY KEY (`idPerfil`);

--
-- Indices de la tabla `Proyecto`
--
ALTER TABLE `Proyecto`
  ADD PRIMARY KEY (`idProyecto`);

--
-- Indices de la tabla `Usuario`
--
ALTER TABLE `Usuario`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_EDD889C192FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_EDD889C1A0D96FBF` (`email_canonical`),
  ADD KEY `fkUsuarioPerfil1idx` (`PerfilidPerfil`);

--
-- Indices de la tabla `UsuarioCurso`
--
ALTER TABLE `UsuarioCurso`
  ADD PRIMARY KEY (`idUsuarioCurso`),
  ADD KEY `fkUsuariohasCursoCurso1idx` (`CursoidCurso`),
  ADD KEY `fkUsuarioCursoUsuario1idx` (`UsuarioidUsuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `Alumno`
--
ALTER TABLE `Alumno`
  MODIFY `idAlumno` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `Contrato`
--
ALTER TABLE `Contrato`
  MODIFY `idContrato` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `ControlAsistencia`
--
ALTER TABLE `ControlAsistencia`
  MODIFY `idControlAsistencia` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `Curso`
--
ALTER TABLE `Curso`
  MODIFY `idCurso` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `GrupoAlimenticio`
--
ALTER TABLE `GrupoAlimenticio`
  MODIFY `idGrupoAlimenticio` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `GrupoAProyecto`
--
ALTER TABLE `GrupoAProyecto`
  MODIFY `idGrupoAProyecto` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `InventarioBodega`
--
ALTER TABLE `InventarioBodega`
  MODIFY `idInventarioBodega` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `Perfil`
--
ALTER TABLE `Perfil`
  MODIFY `idPerfil` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `Proyecto`
--
ALTER TABLE `Proyecto`
  MODIFY `idProyecto` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `Usuario`
--
ALTER TABLE `Usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `UsuarioCurso`
--
ALTER TABLE `UsuarioCurso`
  MODIFY `idUsuarioCurso` int(11) NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `Alumno`
--
ALTER TABLE `Alumno`
  ADD CONSTRAINT `FK_1399D01BC19799F3` FOREIGN KEY (`CursoidCurso`) REFERENCES `Curso` (`idCurso`);

--
-- Filtros para la tabla `ControlAsistencia`
--
ALTER TABLE `ControlAsistencia`
  ADD CONSTRAINT `FK_82475BDAA35C821B` FOREIGN KEY (`InventarioBodegaidInventarioB`) REFERENCES `InventarioBodega` (`idInventarioBodega`),
  ADD CONSTRAINT `FK_82475BDAC19799F3` FOREIGN KEY (`CursoidCurso`) REFERENCES `Curso` (`idCurso`);

--
-- Filtros para la tabla `Curso`
--
ALTER TABLE `Curso`
  ADD CONSTRAINT `FK_BFA6FE84E455CA` FOREIGN KEY (`ProyectoidProyecto`) REFERENCES `Proyecto` (`idProyecto`);

--
-- Filtros para la tabla `GrupoAProyecto`
--
ALTER TABLE `GrupoAProyecto`
  ADD CONSTRAINT `FK_772FBE114E455CA` FOREIGN KEY (`ProyectoidProyecto`) REFERENCES `Proyecto` (`idProyecto`),
  ADD CONSTRAINT `FK_772FBE11935CE485` FOREIGN KEY (`GrupoAlimenticioidGrupoA`) REFERENCES `GrupoAlimenticio` (`idGrupoAlimenticio`);

--
-- Filtros para la tabla `InventarioBodega`
--
ALTER TABLE `InventarioBodega`
  ADD CONSTRAINT `FK_DF936C754E455CA` FOREIGN KEY (`ProyectoidProyecto`) REFERENCES `Proyecto` (`idProyecto`);

--
-- Filtros para la tabla `Usuario`
--
ALTER TABLE `Usuario`
  ADD CONSTRAINT `FK_EDD889C1928FC4B9` FOREIGN KEY (`PerfilidPerfil`) REFERENCES `Perfil` (`idPerfil`);

--
-- Filtros para la tabla `UsuarioCurso`
--
ALTER TABLE `UsuarioCurso`
  ADD CONSTRAINT `FK_2794BE1BC19799F3` FOREIGN KEY (`CursoidCurso`) REFERENCES `Curso` (`idCurso`),
  ADD CONSTRAINT `FK_2794BE1BE3E2172B` FOREIGN KEY (`UsuarioidUsuario`) REFERENCES `Usuario` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
